package net.wizapps.qrscanner.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import net.wizapps.qrscanner.BuildConfig;
import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseDialog;
import net.wizapps.qrscanner.utils.IntentUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class AboutDialog extends BaseDialog {

    @BindView(R.id.versionName) TextView mVersionName;

    @Override
    protected int layoutRes() {
        return R.layout.dialog_about;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        String versionName = "v" + BuildConfig.VERSION_NAME;
        mVersionName.setText(versionName);
    }

    @OnClick({R.id.facebookIcon, R.id.linkedInIcon, R.id.gitLabIcon, R.id.gitHubIcon, R.id.buttonClose})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.facebookIcon:
                loadUrl(R.string.url_facebook);
                break;
            case R.id.linkedInIcon:
                loadUrl(R.string.url_linked_in);
                break;
            case R.id.gitLabIcon:
                loadUrl(R.string.url_git_lab);
                break;
            case R.id.gitHubIcon:
                loadUrl(R.string.url_git_hub);
                break;
            case R.id.buttonClose:
                dismiss();
                break;
        }
    }

    private void loadUrl(int textResId) {
        IntentUtils.webIntent(getActivity(), textResId);
    }
}