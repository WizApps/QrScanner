package net.wizapps.qrscanner.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class ScanLab {

    private static ScanLab sScanLab;
    private final SQLiteDatabase mDatabase;

    private ScanLab(Context context) {
        mDatabase = new ScanDBHelper(context.getApplicationContext()).getWritableDatabase();
    }

    public static ScanLab getInstance(Context context) {
        if (sScanLab == null) {
            synchronized (ScanLab.class) {
                sScanLab = new ScanLab(context);
            }
        }
        return sScanLab;
    }

    public static void destroyInstance() {
        sScanLab = null;
    }

    public void addItem(ScanModel model, boolean restored) {
        insertValues(model, restored);
    }

    public void updateItem(ScanModel model) {
        mDatabase.update(
                ScanContract.TABLE_HISTORY,
                getValues(model, Boolean.TRUE),
                ScanContract._ID + " = ?",
                new String[]{model.getId()});
    }

    public List<ScanModel> getHistory() {
        List<ScanModel> history = new ArrayList<>();
        Cursor cursor = mDatabase.query(ScanContract.TABLE_HISTORY,
                null, null, null, null, null,
                ScanContract._ID + " DESC");

        if (cursor == null) return null;

        ScanCursorWrapper cursorWrapper = new ScanCursorWrapper(cursor);
        cursorWrapper.moveToFirst();
        try {
            while (!cursorWrapper.isAfterLast()) {
                history.add(cursorWrapper.getHistoryItem());
                cursorWrapper.moveToNext();
            }
        } finally {
            cursorWrapper.close();
            cursor.close();
        }

        return history;
    }

    public void restoreHistory(List<ScanModel> history) {
        for (ScanModel model : history) {
            insertValues(model, Boolean.TRUE);
        }
    }

    public void deleteHistory(String id) {
        mDatabase.delete(
                ScanContract.TABLE_HISTORY,
                ScanContract._ID + " = ?",
                new String[]{id});
    }

    public void clearAllHistory() {
        mDatabase.delete(ScanContract.TABLE_HISTORY, null, null);
    }

    private void insertValues(ScanModel model, boolean restored) {
        mDatabase.insert(ScanContract.TABLE_HISTORY, null, getValues(model, restored));
    }

    private ContentValues getValues(ScanModel model, boolean restored) {
        ContentValues values = new ContentValues();
        if (restored) values.put(ScanContract._ID, model.getId());
        values.put(ScanContract.COLUMN_NAME, model.getName());
        values.put(ScanContract.COLUMN_TYPE, model.getType());
        values.put(ScanContract.COLUMN_FORMAT, model.getFormat());
        values.put(ScanContract.COLUMN_DATE, model.getDate());
        values.put(ScanContract.COLUMN_CONTENT, model.getContent());
        return values;
    }
}