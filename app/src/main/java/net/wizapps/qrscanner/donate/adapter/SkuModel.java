package net.wizapps.qrscanner.donate.adapter;

public class SkuModel {

    private final String mSkuId, mPrice;

    public SkuModel(String skuId, String price) {
        mSkuId = skuId;
        mPrice = price;
    }

    String getSkuId() {
        return mSkuId;
    }

    String getPrice() {
        return mPrice;
    }
}