package net.wizapps.qrscanner.donate.adapter;

import android.view.View;
import android.widget.TextView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseHolder;
import net.wizapps.qrscanner.tools.Events;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

class SkuHolder extends BaseHolder {

    @BindView(R.id.skuPrice) TextView mSkuPrice;
    private SkuModel mModel;

    SkuHolder(View view) {
        super(view);
    }

    @OnClick(R.id.donateButton)
    public void onClick() {
        EventBus.getDefault().post(new Events.StartPurchaseEvent(mModel.getSkuId()));
    }

    void bindView(SkuModel model) {
        mModel = model;
        mSkuPrice.setText(model.getPrice());
    }
}