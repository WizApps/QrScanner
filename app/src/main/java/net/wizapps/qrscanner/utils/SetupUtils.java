package net.wizapps.qrscanner.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.dialogs.DonateDialog;
import net.wizapps.qrscanner.tools.Constants;

public final class SetupUtils {

    private SetupUtils() {}

    public static int getLaserColor(Context context) {
        String laserColor = PrefUtils.getLaserColor(context);
        String colorBlue = context.getString(R.string.key_laser_blue);
        String colorRed = context.getString(R.string.key_laser_red);
        String colorGreen = context.getString(R.string.key_laser_green);
        int color;
        if (laserColor.equals(colorBlue)) {
            color = R.color.colorAccent;
        } else if (laserColor.equals(colorRed)) {
            color = R.color.colorRed;
        } else if (laserColor.equals(colorGreen)) {
            color = R.color.colorGreen;
        } else {
            color = R.color.colorPrimary;
        }

        return ContextCompat.getColor(context, color);
    }

    public static boolean getWindowType(Context context) {
        String windowType = PrefUtils.getWindowType(context);
        String typeSquare = context.getString(R.string.pref_window_square);
        if (windowType.equals(typeSquare)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public static void showDonateDialogIfNeeded(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!PrefUtils.isStopShowingDonateDialog(context)) {
                    if (PrefUtils.getStartsCount(context) >= Constants.DONATE_DIALOG_COUNTER) {
                        PrefUtils.setStartsCount(context, Constants.ZERO);
                        showDonateDialog(context);
                    } else {
                        PrefUtils.setStartsCount(context, PrefUtils.getStartsCount(context) + 1);
                    }
                }
            }
        }).start();
    }

    private static void showDonateDialog(final Context context) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                new DonateDialog().show(((AppCompatActivity) context).getSupportFragmentManager(), DonateDialog.class.getSimpleName());
            }
        });
    }
}