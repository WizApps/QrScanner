package net.wizapps.qrscanner.history.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import net.wizapps.qrscanner.data.ScanLab;
import net.wizapps.qrscanner.data.ScanModel;

import java.util.List;

public class HistoryLoader extends AsyncTaskLoader<List<ScanModel>> {

    public static final int HISTORY_LOADER_ID = 111;

    public HistoryLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<ScanModel> loadInBackground() {
        return ScanLab.getInstance(getContext()).getHistory();
    }
}