package net.wizapps.qrscanner.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ShareCompat;

import net.wizapps.qrscanner.R;

public final class IntentUtils {

    private IntentUtils() {}

    static void emailIntent(Context context) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:" + context.getString(R.string.text_email)));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.text_subject));
        emailIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.text_hello));
        checkBeforeLaunching(context, emailIntent);
    }

    static void emailIntent(Context context, String email, String subject, String body) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:" + email));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        checkBeforeLaunching(context, emailIntent);
    }

    static void smsIntent(Context context, String phoneNumber, String smsBody) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + phoneNumber));
        intent.putExtra("sms_body", smsBody);
        checkBeforeLaunching(context, intent);
    }

    static void feedbackIntent(Context context) {
        String url = context.getString(R.string.url_app);
        Intent feedbackIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        checkBeforeLaunching(context, feedbackIntent);
    }

    static void shareIntent(Activity activity) {
        ShareCompat.IntentBuilder
                .from(activity)
                .setType("text/plain")
                .setChooserTitle(activity.getString(R.string.app_name))
                .setText(activity.getString(R.string.url_app))
                .startChooser();
    }

    public static void shareIntent(Activity activity, String text) {
        ShareCompat.IntentBuilder
                .from(activity)
                .setType("text/plain")
                .setText(text)
                .startChooser();
    }

    public static void geoIntent(Context context, String geoLocation) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(geoLocation));
        checkBeforeLaunching(context, intent);
    }

    public static void webIntent(Context context, int textResId) {
        String url = context.getString(textResId);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        checkBeforeLaunching(context, intent);
    }

    public static void webIntent(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        checkBeforeLaunching(context, intent);
    }

    public static void telIntent(Context context, String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(phoneNumber));
        checkBeforeLaunching(context, intent);
    }

    public static void searchIntent(Context context, String query) {
        Uri uri = Uri.parse("http://www.google.com/#q=" + query);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        checkBeforeLaunching(context, intent);
    }

    private static void checkBeforeLaunching(Context context, Intent intent) {
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }
}