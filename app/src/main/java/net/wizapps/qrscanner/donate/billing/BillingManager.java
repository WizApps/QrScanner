package net.wizapps.qrscanner.donate.billing;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.Arrays;
import java.util.List;

public class BillingManager {

    private static final String TAG = BillingManager.class.getSimpleName();
    private final BillingClient mBillingClient;
    private static final List<String> SKUs;

    static {
        SKUs = Arrays.asList(
                BillingConstants.ONE_DOLLAR,
                BillingConstants.TWO_DOLLARS,
                BillingConstants.THREE_DOLLARS,
                BillingConstants.FIVE_DOLLARS,
                BillingConstants.TEN_DOLLARS,
                BillingConstants.TWENTY_DOLLARS,
                BillingConstants.THIRTY_DOLLARS,
                BillingConstants.FIFTY_DOLLARS,
                BillingConstants.HUNDRED_DOLLARS);
    }

    public BillingManager(Context context,
                          BillingSetupListener setupListener,
                          PurchasesUpdatedListener updatedListener) {

        mBillingClient = BillingClient.newBuilder(context).setListener(updatedListener).build();
        startConnectionIfNeeded(setupListener);
    }

    private void startConnectionIfNeeded(final BillingSetupListener setupListener) {
        if (!mBillingClient.isReady()) {
            mBillingClient.startConnection(new BillingClientStateListener() {
                @Override
                public void onBillingSetupFinished(@BillingClient.BillingResponse int responseCode) {
                    if (responseCode == BillingClient.BillingResponse.OK) {
                        setupListener.onConnectionSuccessful();
                    } else {
                        setupListener.onConnectionFailed(responseCode);
                    }
                }

                @Override
                public void onBillingServiceDisconnected() {}
            });
        } else {
            setupListener.onConnectionSuccessful();
        }
    }

    public void querySkuDetailsAsync(final SkuDetailsResponseListener listener) {
        SkuDetailsParams skuDetailsParams = SkuDetailsParams.newBuilder()
                .setSkusList(SKUs)
                .setType(BillingClient.SkuType.INAPP)
                .build();

        mBillingClient.querySkuDetailsAsync(skuDetailsParams, listener);
    }

    public void startPurchaseFlow(Activity activity, String skuId) {
        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setType(BillingClient.SkuType.INAPP)
                .setSku(skuId)
                .build();

        mBillingClient.launchBillingFlow(activity, billingFlowParams);
    }

    public void consumeAsync(final String purchaseToken) {
        final ConsumeResponseListener responseListener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(@BillingClient.BillingResponse int responseCode, String purchaseToken) {
                Log.d(TAG, BillingCodeHandler.handleResponseCode(responseCode));
                Log.d(TAG, purchaseToken);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                mBillingClient.consumeAsync(purchaseToken, responseListener);
            }
        }).start();
    }

    public void disableConnection() {
        if (mBillingClient != null && mBillingClient.isReady()) {
            mBillingClient.endConnection();
        }
    }
}