package net.wizapps.qrscanner.scanner.mvp;

import net.wizapps.qrscanner.base.BasePresenter;

public interface ScannerPresenter extends BasePresenter<ScannerView> {

    void onFlashLightClicked(boolean isFlashLightOn);

    void checkFleshLight(boolean isFlashLightOn);

    void onBackPressed(boolean isDrawerOpen);
}