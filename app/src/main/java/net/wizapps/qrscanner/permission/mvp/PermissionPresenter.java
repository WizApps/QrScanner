package net.wizapps.qrscanner.permission.mvp;

import android.support.annotation.NonNull;

import net.wizapps.qrscanner.base.BasePresenter;

public interface PermissionPresenter extends BasePresenter<PermissionView> {

    void checkPermission(boolean permissionGranted);

    void permissionResult(int requestCode, @NonNull int[] grantResults);
}