package net.wizapps.qrscanner.tools;

public final class Events {

    private Events() {}

    /**
     * DefaultEvent
     **/
    public static class DefaultEvent {}

    /**
     * DonateAmountEvent
     **/
    public static class DonateAmountEvent {}

    /**
     * DeleteHistoryEvent
     **/
    public static class DeleteHistoryEvent {}

    /**
     * UpdateHistoryEvent
     **/
    public static class UpdateHistoryEvent {}

    /**
     * PasswordActivityEvent
     **/
    public static class PasswordActivityEvent {}

    /**
     * RestoreSettingsEvent
     **/
    public static class RestoreSettingsEvent {}

    /**
     * ShowDialogEvent
     **/
    public static class ShowDialogEvent {}

    /**
     * PasswordConditionEvent
     **/
    public static class PasswordConditionEvent {

        private boolean mPasswordAdded;

        public PasswordConditionEvent(boolean passwordAdded) {
            mPasswordAdded = passwordAdded;
        }

        public boolean isPasswordAdded() {
            return mPasswordAdded;
        }
    }

    /**
     * StartPurchaseEvent
     **/
    public static class StartPurchaseEvent {

        private final String mSkuId;

        public StartPurchaseEvent(String skuId) {
            mSkuId = skuId;
        }

        public String getSkuId() {
            return mSkuId;
        }
    }

    /**
     * StopScanningEvent
     **/
    public static class StopScanningEvent {

        private final boolean mStopScanning;

        public StopScanningEvent(boolean stopScanning) {
            mStopScanning = stopScanning;
        }

        public boolean isStopScanning() {
            return mStopScanning;
        }
    }

    /**
     * SwipeHistoryEvent
     **/
    public static class SwipeHistoryEvent {

        private final int mPosition;

        public SwipeHistoryEvent(int position) {
            mPosition = position;
        }

        public int getPosition() {
            return mPosition;
        }
    }
}