package net.wizapps.qrscanner.history;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseActivity;
import net.wizapps.qrscanner.callbacks.HistorySwipeCallback;
import net.wizapps.qrscanner.data.ScanLab;
import net.wizapps.qrscanner.data.ScanModel;
import net.wizapps.qrscanner.dialogs.DeleteDialog;
import net.wizapps.qrscanner.dialogs.LockDialog;
import net.wizapps.qrscanner.dialogs.UnlockDialog;
import net.wizapps.qrscanner.history.adapter.HistoryAdapter;
import net.wizapps.qrscanner.history.loaders.HistoryLoader;
import net.wizapps.qrscanner.history.loaders.HistoryRestoreLoader;
import net.wizapps.qrscanner.password.PasswordActivity;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.tools.Events;
import net.wizapps.qrscanner.utils.InfoUtils;
import net.wizapps.qrscanner.utils.PrefUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class HistoryActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<ScanModel>> {

    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    @BindView(R.id.emptyHistory) View mEmptyView;
    @BindView(R.id.progressBar) View mProgressBar;
    @BindView(R.id.deleteButton) View mDeleteButton;
    @BindView(R.id.passButton) ImageView mPassButton;

    private final HistoryAdapter mAdapter = new HistoryAdapter();
    private List<ScanModel> mHistory;

    @Override
    protected int layoutRes() {
        return R.layout.activity_history;
    }

    @Override
    protected void setUi() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(Boolean.TRUE);
        mRecyclerView.setAdapter(mAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(new HistorySwipeCallback());
        touchHelper.attachToRecyclerView(mRecyclerView);
        getSupportLoaderManager().restartLoader(HistoryLoader.HISTORY_LOADER_ID, null, this);
        if (!TextUtils.isEmpty(PrefUtils.getPassword(this))) {
            mPassButton.setImageResource(R.drawable.ic_unlock);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ScanLab.destroyInstance();
    }

    @NonNull
    @Override
    public Loader<List<ScanModel>> onCreateLoader(int id, Bundle args) {
        mProgressBar.setVisibility(View.VISIBLE);
        if (id == HistoryLoader.HISTORY_LOADER_ID) {
            return new HistoryLoader(this);
        } else {
            return new HistoryRestoreLoader(this, mHistory);
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<ScanModel>> loader, List<ScanModel> history) {
        mProgressBar.setVisibility(View.GONE);
        mAdapter.setHistory(history);
        checkHistory();
        if (loader.getId() == HistoryRestoreLoader.RESTORE_LOADER_ID) {
            InfoUtils.showSnack(this, R.string.text_restore_all);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<ScanModel>> loader) {}

    @Subscribe
    public void onEvent(Events.SwipeHistoryEvent event) {
        ScanModel model = mAdapter.removeItem(event.getPosition());
        ScanLab.getInstance(this).deleteHistory(model.getId());
        restoreSingleHistory(event.getPosition(), model);
        checkHistory();
    }

    @Subscribe
    public void onEvent(Events.DeleteHistoryEvent event) {
        mHistory = mAdapter.removeHistory();
        restoreAllHistory();
        checkHistory();
    }

    @Subscribe
    public void onEvent(Events.UpdateHistoryEvent event) {
        mAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onEvent(Events.PasswordActivityEvent event) {
        startActivity(new Intent(this, PasswordActivity.class));
    }

    @Subscribe(sticky = true)
    public void onEvent(Events.PasswordConditionEvent event) {
        if (event.isPasswordAdded()) {
            mPassButton.setImageResource(R.drawable.ic_unlock);
            InfoUtils.showAlertInfo(this, R.string.pass_added);
        } else {
            mPassButton.setImageResource(R.drawable.ic_lock);
            InfoUtils.showAlertInfo(this, R.string.pass_removed);
            PrefUtils.setPassword(this, Constants.EMPTY);
        }
        EventBus.getDefault().removeStickyEvent(event);
    }

    @OnClick({R.id.backButton, R.id.deleteButton, R.id.passButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                onBackClick();
                break;
            case R.id.passButton:
                onPassClick();
                break;
            case R.id.deleteButton:
                new DeleteDialog().show(getSupportFragmentManager(), DeleteDialog.class.getSimpleName());
                break;
        }
    }

    private void onPassClick() {
        if (TextUtils.isEmpty(PrefUtils.getPassword(this))) {
            new LockDialog().show(getSupportFragmentManager(), LockDialog.class.getSimpleName());
        } else {
            new UnlockDialog().show(getSupportFragmentManager(), UnlockDialog.class.getSimpleName());
        }
    }

    private void restoreSingleHistory(final int position, final ScanModel model) {
        InfoUtils.getSnack(this, R.string.text_remove_single).setAction(R.string.btn_undo, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapter.restoreItem(position, model);
                ScanLab.getInstance(HistoryActivity.this).addItem(model, Boolean.TRUE);
                InfoUtils.showSnack(HistoryActivity.this, R.string.text_restore_single);
                checkHistory();
            }
        }).show();
    }

    private void restoreAllHistory() {
        InfoUtils.getSnack(this, R.string.text_remove_all).setAction(R.string.btn_undo, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressBar.setVisibility(View.VISIBLE);
                getSupportLoaderManager().restartLoader(HistoryRestoreLoader.RESTORE_LOADER_ID, null, HistoryActivity.this);
            }
        }).show();
    }

    private void checkHistory() {
        boolean adapterNotEmpty = mAdapter.getItemCount() > Constants.ZERO;
        mEmptyView.setVisibility(adapterNotEmpty ? View.GONE : View.VISIBLE);
        mDeleteButton.setVisibility(adapterNotEmpty ? View.VISIBLE : View.GONE);
    }
}