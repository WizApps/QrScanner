<resources>
    <string name="app_name">QR Scanner</string>

    <!-- Info text -->
    <string name="text_permission">Sorry, you can\'t use this application without current permission</string>
    <string name="text_history">History is empty</string>
    <string name="text_get_started">Get started saving new codes</string>
    <string name="text_email" translatable="false">info@wizapps.net</string>
    <string name="text_subject" translatable="false">About \"QR Scanner\"</string>
    <string name="text_hello" translatable="false">Hello WizApps Team.\n\n\n</string>
    <string name="text_data_copied">Data copied to clipboard</string>
    <string name="text_data_saved">Data saved to history</string>
    <string name="text_data_error_copied">You already copied this data</string>
    <string name="text_data_error_saved">You already saved this data</string>

    <!-- Buttons -->
    <string name="btn_copy">Copy</string>
    <string name="btn_save">Save</string>
    <string name="btn_email">Email</string>
    <string name="btn_uri">Go to Web</string>
    <string name="btn_geo">Go to Map</string>
    <string name="btn_tel">Call</string>
    <string name="btn_sms">Sms</string>
    <string name="btn_search">Search</string>
    <string name="btn_info">Info</string>
    <string name="btn_close">Close</string>
    <string name="btn_undo">Undo</string>
    <string name="btn_yes">Yes</string>
    <string name="btn_cancel">Cancel</string>

    <!-- About Dialog -->
    <string name="text_about">About App</string>
    <string name="text_version" translatable="false">v0.9.1</string>
    <string name="text_developer" translatable="false">© %1$s by WizApps</string>
    <string name="text_about_message">Fast and light QR-Barcode Scanner with user friendly and intuitive interface. Absolutely free, without any ads and pay content</string>

    <!-- Info Dialog -->
    <string name="text_name">Name:</string>
    <string name="text_name_hint">Enter code name</string>
    <string name="text_name_info">Code name updated</string>
    <string name="text_type">Type:</string>
    <string name="text_format">Format:</string>
    <string name="text_date">Date:</string>
    <string name="text_content">Content:</string>

    <!-- Password -->
    <string name="pass_question_lock">Do you want to add password for history?</string>
    <string name="pass_question_unlock">Do you want to remove password from history?</string>
    <string name="pass_added">Password added</string>
    <string name="pass_removed">Password removed</string>
    <string name="pass_enter">Enter 4 digits password</string>
    <string name="pass_repeat">Please repeat password</string>
    <string name="pass_error">Wrong password</string>
    <string name="pass_error_match">Passwords did\'t match</string>

    <!-- Menu -->
    <string name="menu_result">Scanning result</string>
    <string name="menu_tools">Tools</string>
    <string name="menu_settings">Settings</string>
    <string name="menu_history">History</string>
    <string name="menu_donate">Donate</string>
    <string name="menu_communication">Communication</string>
    <string name="menu_share">Share</string>
    <string name="menu_feedback">Feedback</string>
    <string name="menu_more_apps">More Apps</string>
    <string name="menu_privacy_policy">Privacy Policy</string>

    <!-- Clear/Restore -->
    <string name="text_remove_header">Do you really want to clear all history?</string>
    <string name="text_remove_message">You could delete only one item, just swipe left or right on it</string>
    <string name="text_remove_single">History removed</string>
    <string name="text_restore_single">History restored</string>
    <string name="text_remove_all">All history removed</string>
    <string name="text_restore_all">All history restored</string>

    <!-- Settings UI -->
    <string name="pref_category_main">UI Settings</string>
    <string name="pref_sound_title">Flashlight sound</string>
    <string name="pref_sound_summary">Sound when clicked on flashlight</string>
    <string name="pref_vibration_title">Vibration on result</string>
    <string name="pref_vibration_summary">Vibration when scanner found a code</string>
    <string name="pref_autofocus_title">Autofocus</string>
    <string name="pref_autofocus_summary">Camera autofocus (recommended)</string>
    <string name="pref_laser_title">Laser color</string>
    <string name="pref_laser_blue">Blue</string>
    <string name="pref_laser_red">Red</string>
    <string name="pref_laser_green">Green</string>
    <string name="pref_laser_white">White</string>
    <string name="pref_window_title">Scanner window type</string>
    <string name="pref_window_square" translatable="false">QR Code</string>
    <string name="pref_window_rectangle" translatable="false">Barcode</string>

    <!-- Settings Extra -->
    <string name="pref_category_extra">Extra Settings</string>
    <string name="pref_copy_title">Auto copying</string>
    <string name="pref_copy_summary">Scanner result automatically copying to clipboard</string>
    <string name="pref_save_title">Auto saving</string>
    <string name="pref_save_summary">Scanner result automatically saving to history</string>
    <string name="pref_launch_title">Auto launching</string>
    <string name="pref_launch_summary">If scanner result is web result, it is automatically opened in browser</string>
    <string name="pref_duplicate_title">Auto checking duplicates</string>
    <string name="pref_duplicate_summary">Does not save code that is already in history</string>
    <string name="pref_default_title">Default settings</string>
    <string name="pref_default_summary">Restore default App settings</string>
    <string name="pref_default_dialog">Do you really want to restore default settings?</string>
    <string name="pref_default_restore">Default settings restored</string>

    <!-- Donate -->
    <string name="donate_info_user">Dear user!</string>
    <string name="donate_info_text">We want to remind You that this application and its entire functionality are absolutely free.\n\nIn case You like our App and You want to support the \"WizApps\" team, please choose one of the options below.\n\nRegards</string>
    <string name="donate_btn_money">Amount of money</string>
    <string name="donate_btc_text">Bitcoin wallet</string>
    <string name="donate_eth_text">Ether wallet</string>
    <string name="donate_etc_text">Ether Classic wallet</string>
    <string name="donate_copied">Wallet address copied</string>
    <string name="donate_price">Price:</string>
    <string name="donate_oops">OOPS!</string>
    <string name="donate_no_internet">No internet connection</string>
    <string name="donate_thx">Thanks a lot for Your donation!</string>
    <string name="donate_dialog_main">If you like this App, maybe you want to donate a small amount of money for developers? :)</string>
    <string name="donate_dialog_secondary">It\'s not necessary at all, but it will motivate our team to develop the same user friendly and free applications in the future</string>
    <string name="donate_dialog_show">Never show again</string>
    <string name="donate_address_btc" translatable="false">17c8aRSSHTtU19xUbiH7owQsSBsX8oNsgU</string>
    <string name="donate_address_eth" translatable="false">0x8a13C8b91680424211e7DAb3551A6808eA041B10</string>
    <string name="donate_address_etc" translatable="false">0xE5ac0e30A444E46f3BC9395DF0E90a421ef59BBE</string>

</resources>