package net.wizapps.qrscanner.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.tools.Constants;

public final class PrefUtils {

    private PrefUtils() {}

    public static boolean isSoundOn(Context context) {
        return getPreference(context).getBoolean(context.getString(R.string.key_flashlight), Boolean.TRUE);
    }

    public static boolean isVibrationOn(Context context) {
        return getPreference(context).getBoolean(context.getString(R.string.key_vibrate), Boolean.TRUE);
    }

    public static boolean isAutofocusOn(Context context) {
        return getPreference(context).getBoolean(context.getString(R.string.key_autofocus), Boolean.TRUE);
    }

    public static String getLaserColor(Context context) {
        return getPreference(context).getString(context.getString(R.string.key_laser), context.getString(R.string.key_laser_blue));
    }

    public static String getWindowType(Context context) {
        return getPreference(context).getString(context.getString(R.string.key_window_type), context.getString(R.string.pref_window_square));
    }

    public static String getPassword(Context context) {
        return getPreference(context).getString(context.getString(R.string.key_password), Constants.EMPTY);
    }

    public static void setPassword(Context context, String password) {
        getPreference(context).edit().putString(context.getString(R.string.key_password), password).apply();
    }

    static int getStartsCount(Context context) {
        return getPreference(context).getInt(context.getString(R.string.key_starts_count), Constants.ZERO);
    }

    static void setStartsCount(Context context, int startsCount) {
        getPreference(context).edit().putInt(context.getString(R.string.key_starts_count), startsCount).apply();
    }

    static boolean isStopShowingDonateDialog(Context context) {
        return getPreference(context).getBoolean(context.getString(R.string.key_show_donate), Boolean.FALSE);
    }

    public static void setStopShowingDonateDialog(Context context) {
        getPreference(context).edit().putBoolean(context.getString(R.string.key_show_donate), Boolean.TRUE).apply();
    }

    public static boolean isAutoCopyOn(Context context) {
        return getPreference(context).getBoolean(context.getString(R.string.key_copy), Boolean.FALSE);
    }

    public static boolean isAutoSaveOn(Context context) {
        return getPreference(context).getBoolean(context.getString(R.string.key_save), Boolean.FALSE);
    }

    public static boolean isAutoLaunchOn(Context context) {
        return getPreference(context).getBoolean(context.getString(R.string.key_launch), Boolean.FALSE);
    }

    static boolean isDuplicateCheckingOn(Context context) {
        return getPreference(context).getBoolean(context.getString(R.string.key_duplicate), Boolean.FALSE);
    }

    private static SharedPreferences getPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}