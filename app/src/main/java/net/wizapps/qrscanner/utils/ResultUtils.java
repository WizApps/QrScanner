package net.wizapps.qrscanner.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ResultParser;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.data.ScanLab;
import net.wizapps.qrscanner.data.ScanModel;
import net.wizapps.qrscanner.dialogs.InfoDialog;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.tools.SaveService;

public final class ResultUtils {

    private ResultUtils() {}

    public static String getType(Result result) {
        ParsedResult parsedResult = ResultParser.parseResult(result);
        return parsedResult.getType().toString();
    }

    public static boolean copyResult(Context context, String stringData, boolean showAlert) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(context.getString(R.string.app_name), stringData);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
            if (showAlert) InfoUtils.showAlertInfo(context, R.string.text_data_copied);
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public static boolean saveResult(Context context, ScanModel model, boolean showAlert) {
        if (PrefUtils.isDuplicateCheckingOn(context)) {
            Intent intent = new Intent(context, SaveService.class);
            intent.putExtra(Constants.KEY_SAVE, model);
            context.startService(intent);
        } else {
            ScanLab.getInstance(context).addItem(model, Boolean.FALSE);
        }

        if (showAlert) InfoUtils.showAlertInfo(context, R.string.text_data_saved);
        return Boolean.TRUE;
    }

    public static void showInfo(Context context, ScanModel model, boolean isHistory) {
        FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
        InfoDialog.newInstance(model, isHistory).show(manager, InfoDialog.class.getSimpleName());
    }

    public static void vibrate(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (vibrator != null) {
            vibrator.vibrate(context.getResources().getInteger(R.integer.duration_vibration));
        }
    }
}