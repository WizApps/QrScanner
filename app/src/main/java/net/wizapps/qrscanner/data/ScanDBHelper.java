package net.wizapps.qrscanner.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class ScanDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "qrScannerHistory.db";
    private static final int DATABASE_VERSION = 1;

    ScanDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + ScanContract.TABLE_HISTORY + " (" +
                ScanContract._ID + " INTEGER PRIMARY KEY, " +
                ScanContract.COLUMN_NAME + " TEXT, " +
                ScanContract.COLUMN_TYPE + " TEXT NOT NULL, " +
                ScanContract.COLUMN_FORMAT + " TEXT NOT NULL, " +
                ScanContract.COLUMN_DATE + " INTEGER NOT NULL, " +
                ScanContract.COLUMN_CONTENT + " TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ScanContract.TABLE_HISTORY);
    }
}