package net.wizapps.qrscanner.dialogs;

import android.view.View;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseDialog;
import net.wizapps.qrscanner.utils.MenuUtils;
import net.wizapps.qrscanner.utils.PrefUtils;

import butterknife.OnClick;

public class DonateDialog extends BaseDialog {

    @Override
    protected int layoutRes() {
        return R.layout.dialog_donate;
    }

    @OnClick({R.id.buttonNeverShow, R.id.buttonDonate, R.id.buttonCancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonNeverShow:
                PrefUtils.setStopShowingDonateDialog(getActivity());
                dismiss();
                break;
            case R.id.buttonDonate:
                MenuUtils.showDonate(getActivity());
                dismiss();
                break;
            case R.id.buttonCancel:
                dismiss();
                break;
        }
    }
}