package net.wizapps.qrscanner.permission.mvp;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

import net.wizapps.qrscanner.tools.Constants;

public class PermissionPresenterImpl implements PermissionPresenter {

    private PermissionView mView;

    @Override
    public void bind(PermissionView view) {
        mView = view;
    }

    @Override
    public void unbind() {
        mView = null;
    }

    @Override
    public void checkPermission(boolean permissionGranted) {
        if (permissionGranted) {
            mView.permissionGranted();
        } else {
            mView.permissionFirstStart();
        }
    }

    @Override
    public void permissionResult(int requestCode, @NonNull int[] grantResults) {
        boolean requestCodeTheSame = requestCode == Constants.REQUEST_PERMISSION;
        boolean resultsNotEmpty = grantResults.length > Constants.ZERO;
        boolean permissionGranted = grantResults[Constants.ZERO] == PackageManager.PERMISSION_GRANTED;
        if (requestCodeTheSame && resultsNotEmpty && permissionGranted) {
            mView.permissionGranted();
        } else {
            mView.permissionDeclined();
        }
    }
}