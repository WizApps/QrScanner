package net.wizapps.qrscanner.donate.mvp;

import com.android.billingclient.api.SkuDetails;

import net.wizapps.qrscanner.base.BasePresenter;

import java.util.List;

public interface DonatePresenter extends BasePresenter<DonateView> {

    void convertData(List<SkuDetails> skuDetailsList);
}