package net.wizapps.qrscanner.donate.mvp;

import com.android.billingclient.api.SkuDetails;

import net.wizapps.qrscanner.donate.adapter.SkuModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DonatePresenterImpl implements DonatePresenter {

    private DonateView mView;

    @Override
    public void bind(DonateView view) {
        mView = view;
    }

    @Override
    public void unbind() {
        mView = null;
    }

    @Override
    public void convertData(List<SkuDetails> skuDetailsList) {
        List<SkuModel> skuList = new ArrayList<>();
        for (SkuDetails details : getSortedListByPrice(skuDetailsList)) {
            skuList.add(new SkuModel(details.getSku(), details.getPrice()));
        }
        if (mView != null) mView.dataConverted(skuList);
    }

    private List<SkuDetails> getSortedListByPrice(List<SkuDetails> skuDetailsList) {
        Collections.sort(skuDetailsList, new Comparator<SkuDetails>() {
            @Override
            public int compare(SkuDetails skuOne, SkuDetails skuSecond) {
                if (skuOne.getPriceAmountMicros() > skuSecond.getPriceAmountMicros()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
        return skuDetailsList;
    }
}