package net.wizapps.qrscanner.history.adapter;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseHolder;
import net.wizapps.qrscanner.data.ScanModel;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.utils.DateTimeUtils;
import net.wizapps.qrscanner.utils.ResultUtils;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;

class HistoryHolder extends BaseHolder {

    @BindView(R.id.historyContent) TextView mContent;
    @BindView(R.id.historyDate) TextView mDate;
    @BindView(R.id.historyType) ImageView mType;

    private ScanModel mModel;

    HistoryHolder(View view) {
        super(view);
    }

    @OnClick(R.id.historyView)
    void onClick() {
        ResultUtils.showInfo(itemView.getContext(), mModel, Boolean.TRUE);
    }

    @OnLongClick(R.id.historyView)
    boolean onLongClick() {
        ResultUtils.copyResult(itemView.getContext(), mModel.getContent(), Boolean.TRUE);
        return Boolean.TRUE;
    }

    void bindView(ScanModel model) {
        mModel = model;
        int textColorResId;
        if (!TextUtils.isEmpty(model.getName())) {
            mContent.setText(model.getName());
            textColorResId = R.color.colorAccent;
        } else {
            mContent.setText(model.getContent());
            textColorResId = R.color.colorBlack;
        }
        mContent.setTextColor(ContextCompat.getColor(itemView.getContext(), textColorResId));
        mDate.setText(DateTimeUtils.getFormattedDate(itemView.getContext(), model.getDate()));
        mType.setImageDrawable(getTypeDrawable(model.getType()));
    }

    private Drawable getTypeDrawable(String type) {
        int drawableResId;
        switch (type) {
            case Constants.TYPE_CONTACT:
                drawableResId = R.drawable.ic_contact;
                break;
            case Constants.TYPE_EMAIL:
                drawableResId = R.drawable.ic_email;
                break;
            case Constants.TYPE_PRODUCT:
                drawableResId = R.drawable.ic_product;
                break;
            case Constants.TYPE_URI:
                drawableResId = R.drawable.ic_uri;
                break;
            case Constants.TYPE_GEO:
                drawableResId = R.drawable.ic_geo;
                break;
            case Constants.TYPE_TEL:
                drawableResId = R.drawable.ic_tel;
                break;
            case Constants.TYPE_SMS:
                drawableResId = R.drawable.ic_sms;
                break;
            case Constants.TYPE_VIN:
                drawableResId = R.drawable.ic_product;
                break;
            case Constants.TYPE_ISBN:
                drawableResId = R.drawable.ic_product;
                break;
            case Constants.TYPE_WIFI:
                drawableResId = R.drawable.ic_wifi;
                break;
            case Constants.TYPE_CALENDAR:
                drawableResId = R.drawable.ic_calendar;
                break;
            default:
                drawableResId = R.drawable.ic_text;
                break;
        }
        return ContextCompat.getDrawable(itemView.getContext(), drawableResId);
    }
}