package net.wizapps.qrscanner.callbacks;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.tools.Events;

import org.greenrobot.eventbus.EventBus;

public class HistorySwipeCallback extends ItemTouchHelper.SimpleCallback {

    public HistorySwipeCallback() {
        super(Constants.ZERO, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
    }

    @Override
    public boolean onMove(
            RecyclerView recyclerView,
            RecyclerView.ViewHolder viewHolder,
            RecyclerView.ViewHolder target) {
        return Boolean.FALSE;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        EventBus.getDefault().post(new Events.SwipeHistoryEvent(viewHolder.getAdapterPosition()));
    }
}