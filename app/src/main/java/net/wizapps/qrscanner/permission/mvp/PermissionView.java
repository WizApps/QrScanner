package net.wizapps.qrscanner.permission.mvp;

public interface PermissionView {

    void permissionFirstStart();

    void permissionGranted();

    void permissionDeclined();
}