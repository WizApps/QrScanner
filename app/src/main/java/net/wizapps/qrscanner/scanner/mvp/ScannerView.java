package net.wizapps.qrscanner.scanner.mvp;

public interface ScannerView {

    void switchFlashLight(boolean isFlashLightOn);

    void playFlashLightSound();

    void closeNavDrawer();

    void finishActivity();
}