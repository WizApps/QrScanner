package net.wizapps.qrscanner.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class BaseHolder extends RecyclerView.ViewHolder {

    protected BaseHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}