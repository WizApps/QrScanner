package net.wizapps.qrscanner.settings;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseActivity;
import net.wizapps.qrscanner.dialogs.RestoreDialog;
import net.wizapps.qrscanner.tools.Events;

import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

public class SettingsActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_settings;
    }

    @Override
    protected void setUi() {}

    @OnClick(R.id.backButton)
    public void onBackClick() {
        finish();
    }

    @Subscribe
    public void onEvent(Events.ShowDialogEvent event) {
        new RestoreDialog().show(getSupportFragmentManager(), RestoreDialog.class.getSimpleName());
    }
}