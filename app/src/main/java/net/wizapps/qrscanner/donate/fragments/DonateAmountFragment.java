package net.wizapps.qrscanner.donate.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseFragment;
import net.wizapps.qrscanner.donate.adapter.SkuAdapter;
import net.wizapps.qrscanner.donate.adapter.SkuModel;
import net.wizapps.qrscanner.donate.billing.BillingCodeHandler;
import net.wizapps.qrscanner.donate.billing.BillingManager;
import net.wizapps.qrscanner.donate.billing.BillingSetupListener;
import net.wizapps.qrscanner.donate.mvp.DonatePresenter;
import net.wizapps.qrscanner.donate.mvp.DonatePresenterImpl;
import net.wizapps.qrscanner.donate.mvp.DonateView;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.tools.Events;
import net.wizapps.qrscanner.utils.InfoUtils;
import net.wizapps.qrscanner.utils.PrefUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

public class DonateAmountFragment extends BaseFragment implements
        SkuDetailsResponseListener,
        PurchasesUpdatedListener,
        BillingSetupListener,
        DonateView {

    public static final String TAG = DonateAmountFragment.class.getSimpleName();

    @BindView(R.id.progressBar) View mProgress;
    @BindView(R.id.donateList) RecyclerView mRecyclerView;

    private BillingManager mBillingManager;
    private DonatePresenter mPresenter;
    private SkuAdapter mAdapter;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_donate_amount;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        setupAdapter();
        mPresenter = new DonatePresenterImpl();
        mPresenter.bind(this);
        mBillingManager = new BillingManager(getActivity(), this, this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.unbind();
        mBillingManager.disableConnection();
    }

    @Override
    public void onConnectionSuccessful() {
        if (mAdapter.isEmpty()) {
            mBillingManager.querySkuDetailsAsync(this);
        } else {
            hideProgress();
        }
    }

    @Override
    public void onConnectionFailed(int responseCode) {
        InfoUtils.showAlertError(getActivity(), BillingCodeHandler.handleResponseCode(responseCode));
        hideProgress();
    }

    @Override
    public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
        if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
            mPresenter.convertData(skuDetailsList);
        } else {
            onConnectionFailed(responseCode);
        }
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        if (responseCode == BillingClient.BillingResponse.OK) {
            PrefUtils.setStopShowingDonateDialog(getActivity());
            InfoUtils.showAlertInfo(getActivity(), R.string.donate_thx);
            setupPurchaseForMultipleTimes(purchases);
        } else {
            if (responseCode != BillingClient.BillingResponse.USER_CANCELED) {
                onConnectionFailed(responseCode);
            }
        }
    }

    @Override
    public void dataConverted(List<SkuModel> skuList) {
        mAdapter.setData(skuList);
        hideProgress();
    }

    @Subscribe
    public void onEvent(Events.StartPurchaseEvent event) {
        mBillingManager.startPurchaseFlow(getActivity(), event.getSkuId());
    }

    private void setupPurchaseForMultipleTimes(@Nullable List<Purchase> purchases) {
        if (purchases != null && purchases.size() > Constants.ZERO) {
            for (Purchase purchase : purchases) {
                mBillingManager.consumeAsync(purchase.getPurchaseToken());
            }
        }
    }

    private void setupAdapter() {
        mAdapter = new SkuAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(Boolean.TRUE);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void hideProgress() {
        mProgress.setVisibility(View.INVISIBLE);
    }
}