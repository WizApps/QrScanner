package net.wizapps.qrscanner.scanner;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.zxing.Result;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseActivity;
import net.wizapps.qrscanner.callbacks.DrawerListenerImpl;
import net.wizapps.qrscanner.result.ResultActivity;
import net.wizapps.qrscanner.scanner.mvp.ScannerPresenter;
import net.wizapps.qrscanner.scanner.mvp.ScannerPresenterImpl;
import net.wizapps.qrscanner.scanner.mvp.ScannerView;
import net.wizapps.qrscanner.tools.Events;
import net.wizapps.qrscanner.utils.MenuUtils;
import net.wizapps.qrscanner.utils.PrefUtils;
import net.wizapps.qrscanner.utils.ResultUtils;
import net.wizapps.qrscanner.utils.SetupUtils;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.State;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        ZXingScannerView.ResultHandler,
        ScannerView {

    @BindView(R.id.scannerView) ZXingScannerView mScannerView;
    @BindView(R.id.flashlight) ImageView mFlashlight;
    @BindView(R.id.drawerLayout) DrawerLayout mDrawerLayout;
    @BindView(R.id.navView) NavigationView mNavView;

    @State boolean mIsFlashLightOn;
    @State boolean mStopScanning;

    private final ScannerPresenter mPresenter = new ScannerPresenterImpl();
    private MediaPlayer mPlayer;

    @Override
    protected int layoutRes() {
        return R.layout.activity_scan;
    }

    @Override
    protected void setUi() {
        mPlayer = MediaPlayer.create(this, R.raw.sound_flash);
        mNavView.setNavigationItemSelectedListener(this);
        mDrawerLayout.addDrawerListener(new DrawerListenerImpl() {
            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                mScannerView.setResultHandler(null);
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                if (!mStopScanning) {
                    mScannerView.resumeCameraPreview(ScannerActivity.this);
                }
            }
        });
        MenuUtils.setupDate(this, mNavView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mScannerView.setAutoFocus(PrefUtils.isAutofocusOn(this));
        mScannerView.setLaserColor(SetupUtils.getLaserColor(this));
        mScannerView.setSquareViewFinder(SetupUtils.getWindowType(this));
        mScannerView.startCamera();
        mPresenter.bind(this);
        mPresenter.checkFleshLight(mIsFlashLightOn);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SetupUtils.showDonateDialogIfNeeded(this);
        if (!mStopScanning && !mDrawerLayout.isDrawerOpen(mNavView)) {
            mScannerView.setResultHandler(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mScannerView.stopCamera();
        mPresenter.unbind();
    }

    @Override
    public void onBackPressed() {
        mPresenter.onBackPressed(mDrawerLayout.isDrawerOpen(mNavView));
    }

    @Override
    public void handleResult(Result result) {
        if (PrefUtils.isVibrationOn(this)) ResultUtils.vibrate(this);
        startActivity(ResultActivity.newInstance(this, result));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        mDrawerLayout.closeDrawer(mNavView);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MenuUtils.onNavDrawerClicked(ScannerActivity.this, item.getItemId());
            }
        }, getResources().getInteger(R.integer.duration_delay));
        return Boolean.TRUE;
    }

    @Override
    public void switchFlashLight(boolean isFlashLightOn) {
        mIsFlashLightOn = isFlashLightOn;
        mScannerView.setFlash(isFlashLightOn);
        int imageResId = isFlashLightOn ? R.drawable.ic_flash_on : R.drawable.ic_flash_off;
        mFlashlight.setImageDrawable(ContextCompat.getDrawable(this, imageResId));
    }

    @Override
    public void playFlashLightSound() {
        if (PrefUtils.isSoundOn(this)) mPlayer.start();
    }

    @Override
    public void closeNavDrawer() {
        mDrawerLayout.closeDrawer(mNavView);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Subscribe
    public void onEvent(Events.StopScanningEvent event) {
        mStopScanning = event.isStopScanning();
        if (!mStopScanning) mScannerView.resumeCameraPreview(this);
    }

    @OnClick({R.id.flashlight, R.id.menu})
    public void onToolbarClicked(View view) {
        switch (view.getId()) {
            case R.id.flashlight:
                mPresenter.onFlashLightClicked(!mScannerView.getFlash());
                break;
            case R.id.menu:
                mDrawerLayout.openDrawer(mNavView);
                break;
        }
    }
}