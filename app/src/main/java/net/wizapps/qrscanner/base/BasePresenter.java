package net.wizapps.qrscanner.base;

public interface BasePresenter<T> {

    void bind(T view);

    void unbind();
}