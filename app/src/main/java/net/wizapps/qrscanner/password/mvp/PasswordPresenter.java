package net.wizapps.qrscanner.password.mvp;

import net.wizapps.qrscanner.base.BasePresenter;

public interface PasswordPresenter extends BasePresenter<PasswordView> {

    void getEncryptedPass(String enteredPass);
}