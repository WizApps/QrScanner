package net.wizapps.qrscanner.donate.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.wizapps.qrscanner.R;

import java.util.ArrayList;
import java.util.List;

public class SkuAdapter extends RecyclerView.Adapter<SkuHolder> {

    private List<SkuModel> mSkuList = new ArrayList<>();

    @NonNull
    @Override
    public SkuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sku, parent, Boolean.FALSE);
        return new SkuHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SkuHolder holder, int position) {
        holder.bindView(mSkuList.get(position));
    }

    @Override
    public int getItemCount() {
        return mSkuList.size();
    }

    public boolean isEmpty() {
        return mSkuList.isEmpty();
    }

    public void setData(List<SkuModel> skuList) {
        mSkuList.clear();
        mSkuList.addAll(skuList);
        notifyDataSetChanged();
    }
}