package net.wizapps.qrscanner.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.tools.Events;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.ButterKnife;
import icepick.Icepick;

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract int layoutRes();

    protected abstract void setUi();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutRes());
        Icepick.restoreInstanceState(this, savedInstanceState);
        ButterKnife.bind(this);
        setUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void onBackPressed() {
        onBackClick();
    }

    @Subscribe
    public void onEvent(Events.DefaultEvent event) {
        // default event for avoid crashes in activities witch did't handle any events
    }

    protected void addFragment(Fragment fragment) {
        if (getSupportFragmentManager().findFragmentById(R.id.donateContainer) == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.donateContainer, fragment)
                    .commit();
        }
    }

    protected void replaceFragment(Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.donateContainer, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    protected void onBackClick() {
        if (getSupportFragmentManager().getBackStackEntryCount() > Constants.ZERO) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }
}