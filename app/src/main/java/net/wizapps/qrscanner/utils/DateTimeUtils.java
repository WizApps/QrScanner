package net.wizapps.qrscanner.utils;

import android.content.Context;
import android.text.format.DateFormat;

import net.wizapps.qrscanner.tools.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class DateTimeUtils {

    private DateTimeUtils() {}

    public static String getFormattedDate(Context context, long milliseconds) {
        String timeFormat;
        Locale locale;
        if (DateFormat.is24HourFormat(context)) {
            timeFormat = Constants.INFO_DATE_FORMAT_UK;
            locale = Locale.UK;
        } else {
            timeFormat = Constants.INFO_DATE_FORMAT_US;
            locale = Locale.US;
        }
        return new SimpleDateFormat(timeFormat, locale).format(new Date(milliseconds));
    }
}