package net.wizapps.qrscanner.result.mvp;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.tools.Constants;

public class ResultPresenterImpl implements ResultPresenter {

    private ResultView mView;

    @Override
    public void bind(ResultView view) {
        mView = view;
    }

    @Override
    public void unbind() {
        mView = null;
    }

    @Override
    public void checkResultType(String resultType) {
        switch (resultType) {
            case Constants.TYPE_EMAIL:
                mView.showEmail();
                break;
            case Constants.TYPE_URI:
                mView.showUri();
                break;
            case Constants.TYPE_GEO:
                mView.showGeo();
                break;
            case Constants.TYPE_TEL:
                mView.showTel();
                break;
            case Constants.TYPE_SMS:
                mView.showSms();
                break;
            case Constants.TYPE_ISBN:
                mView.showSearch();
                break;
            case Constants.TYPE_VIN:
                mView.showSearch();
                break;
            case Constants.TYPE_PRODUCT:
                mView.showSearch();
                break;
        }
    }

    @Override
    public void onCopyClicked(boolean isCopied) {
        if (isCopied) {
            mView.showError(R.string.text_data_error_copied);
        } else {
            mView.copyResult();
        }
    }

    @Override
    public void onSaveClicked(boolean isSaved) {
        if (isSaved) {
            mView.showError(R.string.text_data_error_saved);
        } else {
            mView.saveResult();
        }
    }
}