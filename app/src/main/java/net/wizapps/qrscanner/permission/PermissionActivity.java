package net.wizapps.qrscanner.permission;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.permission.mvp.PermissionPresenter;
import net.wizapps.qrscanner.permission.mvp.PermissionPresenterImpl;
import net.wizapps.qrscanner.permission.mvp.PermissionView;
import net.wizapps.qrscanner.scanner.ScannerActivity;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.utils.InfoUtils;

public class PermissionActivity extends AppCompatActivity implements PermissionView {

    private static final String mCameraPermission = android.Manifest.permission.CAMERA;
    private final PermissionPresenter mPresenter = new PermissionPresenterImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.bind(this);
        mPresenter.checkPermission(ContextCompat.checkSelfPermission(this, mCameraPermission) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unbind();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mPresenter.permissionResult(requestCode, grantResults);
    }

    @Override
    public void permissionFirstStart() {
        ActivityCompat.requestPermissions(this, new String[]{mCameraPermission}, Constants.REQUEST_PERMISSION);
    }

    @Override
    public void permissionGranted() {
        startActivity(new Intent(this, ScannerActivity.class));
        finish();
    }

    @Override
    public void permissionDeclined() {
        InfoUtils.showError(this, R.string.text_permission);
        finish();
    }
}