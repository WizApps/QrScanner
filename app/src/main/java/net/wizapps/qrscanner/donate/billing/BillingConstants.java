package net.wizapps.qrscanner.donate.billing;

interface BillingConstants {

    // SKUs
    String ONE_DOLLAR = "one_dollar";
    String TWO_DOLLARS = "two_dollars";
    String THREE_DOLLARS = "three_dollars";
    String FIVE_DOLLARS = "five_dollars";
    String TEN_DOLLARS = "ten_dollars";
    String TWENTY_DOLLARS = "twenty_dollars";
    String THIRTY_DOLLARS = "thirty_dollars";
    String FIFTY_DOLLARS = "fifty_dollars";
    String HUNDRED_DOLLARS = "hundred_dollars";

    // Code handlers
    String SUCCESS = "Success";
    String FEATURE_NOT_SUPPORTED = "Requested feature is not supported by Play Store on the current device";
    String SERVICE_DISCONNECTED = "Play Store service is not connected now - potentially transient state";
    String USER_CANCELED = "User pressed back or canceled a dialog";
    String SERVICE_UNAVAILABLE = "Network connection is down";
    String BILLING_UNAVAILABLE = "Billing API version is not supported for the type requested";
    String ITEM_UNAVAILABLE = "Requested product is not available for purchase";
    String DEVELOPER_ERROR = "Invalid arguments provided to the API";
    String ERROR = "Fatal error during the API action";
    String ITEM_ALREADY_OWNED = "Failure to purchase since item is already owned";
    String ITEM_NOT_OWNED = "Failure to consume since item is not owned";
}